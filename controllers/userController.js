const bcrypt = require("bcrypt");
// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");

// Check if the email already exists
    /*
        Steps: 
        1. Use mongoose "find" method to find duplicate emails
        2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
    */

const checkEmailExists = async (reqBody) => {
    try {
        const foundUser = await User.find({email: reqBody.email});
        if (foundUser.length > 0){
            return true;
        } else {
            return false;
        // No duplicate email found
        // The user is not yet registered in the database
        }
    } catch(err) {
        console.log(err);
        return err;
    };
};

// User registration
    /*
        Steps:
        1. Create a new User object using the mongoose model and the information from the request body
        2. Make sure that the password is encrypted
        3. Save the new User to the database
    */

const registerUser = async (reqBody) => {
    try {
        let newUser = new User({
            firstName: reqBody.firstName,
            lastName: reqBody.lastName,
            email: reqBody.email,
            mobileNo: reqBody.mobileNo,
            // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
            password: bcrypt.hashSync(reqBody.password, 10)
        });
        const savedUser = await newUser.save();
        if (savedUser) {
            return true;
        } else {
            return false;
        };
    } catch(err) {
        console.log(err);
        return err;
    };
};

// User authentication
    /*
        Steps:
        1. Check the database if the user email exists
        2. Compare the password provided in the login form with the password stored in the database
        3. Generate/return a JSON web token if the user is successfully logged in and return false if not
    */
const loginUser = async (reqBody)=>{
    try {
        const foundUser = await User.findOne({email: reqBody.email});
        if(foundUser){
            // Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            const isPasswordCorrect = await bcrypt.compare(reqBody.password, foundUser.password);
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(foundUser)};
            } else {
                return false;
            }
        } else {
            return false;
        };
    } catch(err){
        console.log(err);
        return err;
    };
};

const getProfile = async (data)=>{
    try {
        const foundUser = await User.findById(data.userId);
        console.log(foundUser);
        if(foundUser){
            foundUser.password = "";
            return foundUser;
        } else {
            return false;
        };
    } catch(err){
        return err;
    };
};

// Enroll user to a course
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Add the course ID to the user's enrollment array
    3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
const enroll = async (data) => {
    // Add the course ID in the enrollments array of the user
    // Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
    // Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
    try {
        const isUserUpdated = await User.findById(data.userId)
        .then(async user => {
            user.enrollments.push({courseId : data.courseId});
            try {
                const updatedUser = await user.save();
                return true;
            } catch (err) { 
                return false;
            }
        });

        const isCourseUpdated = await Course.findById(data.courseId)
        .then(async course => {
            // Adds the userId in the course's enrollees array
            if (data.courseId)
            course.enrolless.push({userId: data.userId});

            try {
                const updatedCourse = await course.save();
                return true;
            } catch (err) {
                return false;
            }
        });

        // Condition that will check if the user and course documents have been updated
	    // User enrollment successful
        if(isUserUpdated && isCourseUpdated){
            return true;
        } else {
            return false;
        };
    } catch(err){
        return err;
    };
};

module.exports = {checkEmailExists, registerUser, loginUser, getProfile, enroll};