const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    name : {
        type: String,
        require: [true, "Course is required"]
    },
    description: {
        type: String,
        require: [true, "Description is required"]
    },
    price: {
        type: Number,
        require: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    enrolless: [
        {
           userId: {
            type: String,
            require: [true, "UserId os required"]
           } ,
           enrolledOn: {
            type: Date,
            default: new Date()
           }
        }
    ]
});

module.exports = mongoose.model("Course", courseSchema);