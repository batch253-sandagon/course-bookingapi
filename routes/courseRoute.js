const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    courseController.addCourse(req.body, userData.isAdmin).then(result => res.send(result));
});

// Routes for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        courseController.getAllCourses(userData.isAdmin).then(result => res.send(result));
    } else {
        res.send(false);
    };
});

router.get("/", (req, res) => {
    courseController.getAllActive().then(result => res.send(result));
});

// Retrieving a specific course
/*
    Steps:
    1. Retrieve the course that matches the course ID provided from the URL
*/

router.get("/:id", (req, res) => {
    courseController.getCourse(req.params.id).then(result => res.send(result));
});

router.put("/:id", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        courseController.updateCourse(req.body, req.params.id).then(result => res.send(result));
    } else {
        res.send(false);
    }
    
});

router.patch("/:id/archive", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        courseController.archiveCourse(req.body, req.params.id).then(result => res.send(result));
    } else {
        res.send(false);
    };
});

module.exports = router;