const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res)=>{
    userController.checkEmailExists(req.body).then(result => res.send(result));
});

router.post("/register", (req, res)=>{
    userController.registerUser(req.body).then(result => res.send(result));
});

router.post("/login", (req, res)=>{
    userController.loginUser(req.body).then(result => res.send(result));
});

router.get("/details", (req, res)=>{
    /*userController.getProfile(req.body).then(result => res.send(result));*/

    // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
    const userData = auth.decode(req.headers.authorization);

    userController.getProfile({userId: userData.id}).then(
        result => res.send(result));
});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        courseId: req.body.courseId
    }

    if(!userData.isAdmin){
        userController.enroll(data).then(result => res.send(result));
    } else {
        res.send("Get out of here, admin.");
    };

    
});

module.exports = router;